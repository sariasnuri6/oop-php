<?php

    spl_autoload_register(function($class_name){
        include $class_name . '.php';
    });

    $sheep = new Animal("shaun");

    echo $sheep->name; // "shaun"
    echo $sheep->legs; // 2
    echo $sheep->cold_blooded; // false
    echo "<br>";

    $sungokong = new Ape ("kera sakti");
    echo $sungokong->yell() ; // "Auooo"
    echo "<br>";

    $kodok = new Frog("buduk");
    $kodok->jump() ; // "hop hop"

?>