<?php

    spl_autoload_register(function($class_name){
        include $class_name . '.php';
    });

    class Ape extends Animal{
        
        public function yell() {
            echo 'Auooo';
        }
    }
?>