<?php

    spl_autoload_register(function($class_name){
        include $class_name . '.php';
    });


    class Frog extends Animal{
        public $legs = 4;
        public function jump() {
            echo 'hop hop';
        }
    }
?>