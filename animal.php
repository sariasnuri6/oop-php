<?php

    spl_autoload_register(function($class_name){
        include $class_name . '.php';
    });

    class Animal {
        public $name;
        public $legs = 2;
        public $cold_blooded = "false";
        public function __construct($name){
            $this->name = $name;
        } 
    }

    class Ape extends Animal{
        
        public function yell() {
            echo 'Auooo';
        }
    }
?>